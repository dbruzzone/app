export APP_PWD=password

db() {
  #docker run -t postgres:latest
  
  docker run -P --volumes-from app_data --name app_db -e POSTGRES_USER=app_user -e POSTGRES_PASSWORD=$APP_PWD -d -t postgres:latest
}

app() {
  #docker stop app
  #docker rm app
  docker run -p 3000:3000 --link app_db:postgres josemotanet/app #--name app josemotanet/app
}

# Action is the first argument
action=$1

# Call action
${action} # Example: sh script.sh db
